﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Mapsui.Geometries;
using Mapsui.Layers;
using Mapsui.Projection;
using Mapsui.Providers;
using Mapsui.Styles;
using Mapsui.Utilities;
using Xamarin.Forms;

namespace mapsuiTrack
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            //set to SW rendering
            Mapsui.UI.Forms.MapControl.UseGPU = false;

            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            ShowOnlinelineMap();

            ShowMapTrack();
        }

        public void ShowOnlinelineMap()
        {
            var map = new Mapsui.Map
            {
            };

            //use online layer
            var tileLayer = OpenStreetMap.CreateTileLayer();

            map.Layers.Add(tileLayer);

            var centerOfMap = new Mapsui.Geometries.Point(45.79256294942164, 15.948102057991743);
            // OSM uses spherical mercator coordinates. So transform the lon lat coordinates to spherical mercator
            var sphericalMercatorCoordinate = SphericalMercator.FromLonLat(centerOfMap.Y, centerOfMap.X); //This is Longitude + Latitude (inversed)
            map.Home = n => n.NavigateTo(sphericalMercatorCoordinate, map.Resolutions[13]);

            mapView.Map = map;
        }

        public async void ShowMapTrack()
        {
            var map = mapView.Map;

            //add tracks
            List<MemoryLayer> trackLayers = addRoute();
            foreach (var trackLayer in trackLayers)
            {
                trackLayer.Name = "ImotaRoute";
                trackLayer.IsMapInfoLayer = true;
                map.Layers.Insert(2, trackLayer);
                Thread.Sleep(50);
            }
            //mapView.RefreshGraphics();
            mapView.Refresh();
        }

        private static List<MemoryLayer> addRoute()
        {
            string routePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "activity.gpx");
            Assembly assembly = IntrospectionExtensions.GetTypeInfo(typeof(App)).Assembly;
            Stream embeddedRouteStream = assembly.GetManifestResourceStream("mapsuiTrack.activity.gpx"); // NameOfProgram.NameOfFile.FileExtension

            if (!File.Exists(routePath))
            {
                FileStream fileStreamToWrite = File.Create(routePath);
                embeddedRouteStream.Seek(0, SeekOrigin.Begin);
                embeddedRouteStream.CopyTo(fileStreamToWrite);
                fileStreamToWrite.Close();
            }

            //open GPX file
            XmlDocument gpxDoc = new XmlDocument();
            gpxDoc.Load(routePath);

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(gpxDoc.NameTable);
            nsmgr.AddNamespace("x", "http://www.topografix.com/GPX/1/1");
            //XmlNodeList xmlTrack = gpxDoc.SelectNodes("//x:trk", nsmgr);
            XmlNodeList xmlSegments = gpxDoc.SelectNodes("//x:trkseg", nsmgr);
            List<MemoryLayer> mapLayers = new List<MemoryLayer>();
            List<Feature> features = new List<Feature>();
            Int32 segNo = 0;
            foreach (XmlLinkedNode xmlSegment in xmlSegments)
            {
                //get trackpoints from segment
                XmlNodeList xmlPoints = xmlSegment.SelectNodes("*");
                //var lineString = new LineString();
                List<Mapsui.Geometries.Polygon> routePolygons = new List<Mapsui.Geometries.Polygon>();
                List<Mapsui.Geometries.Point> segmentPoints = new List<Mapsui.Geometries.Point>();
                foreach (XmlElement xelement in xmlPoints)
                {
                    segmentPoints.Add(SphericalMercator.FromLonLat(Convert.ToDouble(xelement.GetAttribute("lon")), Convert.ToDouble(xelement.GetAttribute("lat"))));
                }
                //add layer
                var feature = new Feature()
                {
                    Geometry = new LineString(segmentPoints)
                };

                Mapsui.Styles.Color xSegmentColor = null;
                if (segNo % 2 == 0)
                {
                    //xSegmentColor = new Mapsui.Styles.Color(new Mapsui.Styles.Color(148, 62, 213));
                    xSegmentColor = new Mapsui.Styles.Color(new Mapsui.Styles.Color(250, 70, 115));
                }
                else
                {
                    //xSegmentColor = new Mapsui.Styles.Color(new Mapsui.Styles.Color(51, 255, 51));
                    xSegmentColor = new Mapsui.Styles.Color(new Mapsui.Styles.Color(156, 17, 52));
                }
                feature.Styles.Add(new VectorStyle
                {
                    Line = new Pen
                    {
                        Color = xSegmentColor,
                        PenStrokeCap = PenStrokeCap.Round,
                        PenStyle = PenStyle.Solid,
                        Width = 10,
                    },
                    //Opacity = 0.7f,
                    //Fill = null
                    Fill = new Mapsui.Styles.Brush { Color = xSegmentColor }

                });

                //add feature
                features.Add(feature);

                //add layer
                MemoryLayer mapLayer = new MemoryLayer
                {
                    Name = "Track",
                    DataSource = new MemoryProvider(features),
                };
                mapLayers.Add(mapLayer);

                segNo++;
            }


            return mapLayers;
        }
    }
}
